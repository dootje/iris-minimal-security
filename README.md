# IRIS with minimal security

For development or training purposes it is often convenient to remove as much security features from IRIS as possible. This repo does exactly that:

* Users Admin, SuperUser, _SYSTEM and - especially - UnknownUser get the `%All` role.
* Passwords do not expire.
* All web applications are set to `Unauthenticated`.


### Build

```
docker build -t iris-unsecure:0.1 .
```


### Run

```
docker-compose up --detach
```


### Use

Open [http://localhost:52773/csp/sys/UtilHome.csp](http://localhost:52773/csp/sys/UtilHome.csp) and notice that no credentials are needed to access the Management Portal.
