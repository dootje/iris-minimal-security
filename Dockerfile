ARG IMAGE=docker.iscinternal.com/intersystems/iris:2020.3.0-released

FROM ${IMAGE}

USER irisowner

ENV INIT_DIR=/home/irisowner
COPY init.* ${INIT_DIR}/
RUN ${INIT_DIR}/init.sh
